import React, { Component } from 'react'




const initialState = {
  name:'', email:'',birthday:'',age:'', password:'',gender:'',records:[]
};
export default class FormComponent extends Component {
  constructor(props){
    super(props)
    this.state = initialState
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }
  
  handleSubmit(event){
    const {  name,email,birthday, age, password, gender } = this.state
    event.preventDefault()
    const record = this.state.records;
    record.push({name,email,birthday, age, password, gender})
    this.setState({records: record})
      
  }
  
  
  handleChange(event){
    this.setState({
      [event.target.name] : event.target.value
    })
  }
  
  
  
  render() {

    let dataShow = this.state.records.map((value, index) => {
      return (
        <tr key={index}>
          <td>{value.name}</td>
          <td>{value.email}</td>
          <td>{value.birthday}</td>
          <td>{value.age}</td>
          <td>{value.gender}</td>
        </tr>
      )
    })
    return(
      <>
        <div className="container">
        <div className="title">
          <h1>Registration Form</h1>
        </div>
        <form onSubmit={(e) => this.handleSubmit(e)}>
          <div className="name">
            <label
              htmlFor="userName"
            >
              userName
            </label>
            <br />
            <input type="text" name="name"
              id="name"
              value = {this.state.name}
              onChange={this.handleChange}
              required={true}/>
          </div>
          <div className="Email">
            <label htmlFor="Email">Email</label>
            <br />
            <input
              name='email'
              type="email"
              value = {this.state.email}
            onChange={this.handleChange}
              required={true}
            />
          </div>
          <div className="dob">
            <label htmlFor="Dob">Birthday</label>
            <br />
            <input
              type="date"
              name="birthday"
              value = {this.state.birthday}
            onChange={this.handleChange}
              required={true}
            />
          </div>
          <div className="Age">
            <label htmlFor="age">Age</label>
            <br />
            <input
              type="number"
              name="age"
              value = {this.state.age}
            onChange={this.handleChange}
              required={true}
            />
          </div>
          <div className="Password">
            <label htmlFor="Password">password</label>
            <br />
            <input
            name='password'
              type="password"
              value = {this.state.password}
              onChange={this.handleChange}
              required={true}
            />
          </div>
          <div className="gender">
            <label htmlFor="Gender">gender</label>
            <br />
            <div className="gender-input">
              <input type="radio"  name="gender" value = "male"
            onChange={this.handleChange}
              required={true} />
              <span>Male</span>
              <input type="radio"  name="gender" value = "female"
            onChange={this.handleChange}
              required={true} />
              <span>female</span>
            </div>
          </div>
          <div className="btn">
            <button type="submit">Submit</button>
          </div>
        </form>
      </div>

<div className="container">
<table className="table">
  <thead>
    <tr>
      <th>UserName</th>
      <th>Email</th>
      <th>BirthDay</th>
      <th>Age</th>
      <th>Gender</th>
    </tr>
  </thead>
  <tbody>{dataShow}</tbody>
</table>
</div>
</>
    )
  }
}
